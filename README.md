# app-tiki-manager-core

[![pipeline status](https://gitlab.com/tikiwiki/app-tiki-manager-core/badges/master/pipeline.svg)](https://gitlab.com/tikiwiki/app-tiki-manager-core/commits/master)

This repository allows to build RPM package of Tiki Manager for ClearOS.

## Download latest RPM
[Download app-tiki-manager-core](https://gitlab.com/tikiwiki/app-tiki-manager-core/-/jobs/artifacts/master/download?job=rpm-clearos)

## Build RPM package
```
bash packaging/rpm-builder.sh
```
