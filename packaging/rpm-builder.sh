#!/usr/bin/env bash

RPMBUILDER_FOLDER=/root/rpmbuild
CWD=$(dirname "$(realpath -s $0)")

mkdir -p $RPMBUILDER_FOLDER/SOURCES
mkdir -p $RPMBUILDER_FOLDER/SPECS

cp $CWD/app-tiki-manager-core.spec $RPMBUILDER_FOLDER/SPECS/
cp $CWD/tiki-manager.htaccess $RPMBUILDER_FOLDER/SOURCES/

cd $RPMBUILDER_FOLDER
spectool -g -R SPECS/app-tiki-manager-core.spec
rpmbuild -ba SPECS/app-tiki-manager-core.spec

mkdir -p ${CWD}/rpms/
cp $RPMBUILDER_FOLDER/RPMS/x86_64/*.rpm ${CWD}/rpms/

