Name:           app-tiki-manager-core
Version:        1.0
Release:        1%{?dist}
Summary:        Tiki manager installer
License:        MPLv2
URL:            https://gitlab.com/tikiwiki/tiki-manager/
Source0:        https://gitlab.com/tikiwiki/tiki-manager/-/archive/master/tiki-manager-master.tar.gz
Source1:        tiki-manager.htaccess
BuildRequires:  rh-php71-php-cli
BuildRequires:  rh-php71-php-mbstring
BuildRequires:  rh-php71-php-pdo
BuildRequires:  rh-php71-php-xml
Requires:       rsync
Requires:       openssh-clients
Requires:       git
Requires:       subversion
Requires:       bzip2
Requires:       tar
Requires:       app-mariadb
Requires:       app-web-server

%define php_binary /opt/rh/rh-php71/root/usr/bin/php

%description
app-tiki-manager-core was designed to be used with ClearOS in order to create
a tiki-manager instance along with some basic configurations, including
tiki-manager's web manager enabled by default.

%prep
tar -zxf %{SOURCE0}

%build
# Force PHP 7.1 to run
touch .phpenv; echo "71" > .phpenv
cd tiki-manager-master

# Get composer
EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
%{php_binary} -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE="$(%{php_binary} -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

%{php_binary} composer-setup.php --quiet
rm composer-setup.php

%install
cd tiki-manager-master
mkdir -p %{buildroot}/opt/tiki-manager/{bin,app,webroot}
touch %{buildroot}/opt/tiki-manager/webroot/.phpenv; echo "71" > %{buildroot}/opt/tiki-manager/webroot/.phpenv
touch %{buildroot}/opt/tiki-manager/app/.phpenv; echo "71" > %{buildroot}/opt/tiki-manager/app/.phpenv
mkdir -p %{buildroot}/opt/tiki-manager/app/{backup,data}

# "Manual" webroot configuration
ssh-keygen -t rsa -f %{buildroot}/opt/tiki-manager/app/data/id_rsa -q -P ""
cp -a www/* %{buildroot}/opt/tiki-manager/webroot/
cp composer.phar %{buildroot}/opt/tiki-manager/webroot/
%{php_binary} composer.phar --no-dev --working-dir=%{buildroot}/opt/tiki-manager/webroot/ install

#Move tiki-manager into app folder
cp -r . %{buildroot}/opt/tiki-manager/app
%{php_binary} composer.phar --no-dev --working-dir=%{buildroot}/opt/tiki-manager/app/ install
ln -s /opt/tiki-manager/app/tiki-manager %{buildroot}/opt/tiki-manager/bin/tiki-manager

# Data folders symlinks
mkdir -p %{buildroot}/var/opt/tiki-manager/{backup,cache,data,logs,tmp}
ln -s /opt/tiki-manager/app/backup %{buildroot}/var/opt/tiki-manager/backup
ln -s /opt/tiki-manager/app/cache %{buildroot}/var/opt/tiki-manager/cache
ln -s /opt/tiki-manager/app/data %{buildroot}/var/opt/tiki-manager/data
ln -s /opt/tiki-manager/app/logs %{buildroot}/var/opt/tiki-manager/logs
ln -s /opt/tiki-manager/app/tmp %{buildroot}/var/opt/tiki-manager/tmp

# Configure our webmanager alias into apache
install -Dpm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/httpd/conf.d/tiki-manager.conf

%files

%attr(-,root,root) /opt/tiki-manager
%attr(-,root,root) /var/opt/tiki-manager/
%attr(-,root,root) %{_sysconfdir}/httpd/conf.d/tiki-manager.conf

%post
cd /opt/tiki-manager/app
adminPwd=$(openssl rand -base64 14)
%{php_binary} tiki-manager webmanager:enable --path=/opt/tiki-manager/webroot/ --username=admin --password=$adminPwd --install --no-interaction > /dev/null 2>&1
chown -R apache:apache /opt/tiki-manager/webroot
chmod 766 /opt/tiki-manager/webroot/config.php
chown -R apache:apache /opt/tiki-manager/app/logs
chown -R apache:apache /opt/tiki-manager/app/cache
chown apache:allusers /var/www/virtual
printf "Tiki-manager password generated automatically, it can be found at '/opt/tiki-manager/webroot/config.php'.\n\n"

%changelog
* Tue Dec 17 2019 Softstart <info@softstart.io>
Automatic SSH key pair generation
* Mon Oct 28 2019 Softstart <info@softstart.io>
Initial rpm spec version
